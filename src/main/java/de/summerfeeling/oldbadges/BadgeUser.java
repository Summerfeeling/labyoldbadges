package de.summerfeeling.oldbadges;

import net.labymod.user.User;
import net.labymod.user.group.LabyGroup;

import java.util.UUID;

public class BadgeUser extends User {
	
	public BadgeUser(UUID uuid) {
		super(uuid);
	}
	
	@Override
	public LabyGroup getGroup() {
		LabyGroup group = super.getGroup();
		
		if (group.getId() == BadgeUserManager.DEFAULT_GROUP.getId()) {
			if (getUserManager().isWhitelisted(getUuid())) {
				return BadgeUserManager.DONATOR_GROUP;
			} else {
				return BadgeUserManager.DEFAULT_GROUP;
			}
		}
		
		return group;
	}
}
