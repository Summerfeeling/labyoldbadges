package de.summerfeeling.oldbadges;

import de.summerfeeling.oldbadges.reflect.Reflect;
import net.labymod.user.User;
import net.labymod.user.UserManager;
import net.labymod.user.group.GroupManager;
import net.labymod.user.group.LabyGroup;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BadgeUserManager extends UserManager {
	
	public static final LabyGroup DEFAULT_GROUP = new LabyDefaultGroup();
	public static final LabyGroup DONATOR_GROUP = new LabyGroup(DEFAULT_GROUP.getId(), "user", "User", "", 'f', "USER", "NONE", null, null);
	private Map<UUID, BadgeUser> users = new HashMap<>();
	
	public BadgeUserManager() {
		Reflect.getField(GroupManager.class, "DEFAULT_GROUP").setStatic(DEFAULT_GROUP);
	}
	
	@Override
	public User getUser(UUID uuid) {
		BadgeUser user = users.get(uuid);
		
		if (user == null) {
			this.users.put(uuid, user = new BadgeUser(uuid));
		}
		
		return user;
	}
	
}
