package de.summerfeeling.oldbadges;

import de.summerfeeling.oldbadges.reflect.Reflect;
import net.labymod.api.LabyModAddon;
import net.labymod.main.LabyMod;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.support.util.Debug;
import net.labymod.support.util.Debug.EnumDebugMode;

import java.util.List;

public class OldBadges extends LabyModAddon {
	
	public void onEnable() {
		Reflect.getField(LabyMod.class, "userManager").set(LabyMod.getInstance(), new BadgeUserManager());

		LabyMod.getInstance().getUserManager().init(LabyMod.getInstance().getPlayerUUID(), success -> {
			if (success) {
				Debug.log(EnumDebugMode.USER_MANAGER, "Successfully loaded all userdata (BadgeUserManager)");
			} else {
				Debug.log(EnumDebugMode.USER_MANAGER, "An error occured while loading all userdata (BadgeUserManager)");
			}
		});
	}
	
	public void loadConfig() {
	
	}
	
	protected void fillSettings(List<SettingsElement> list) {
	
	}
}
