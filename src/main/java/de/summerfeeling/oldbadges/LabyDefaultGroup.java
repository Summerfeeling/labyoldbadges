package de.summerfeeling.oldbadges;

import net.labymod.main.LabyMod;
import net.labymod.main.ModTextures;
import net.labymod.user.group.LabyGroup;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class LabyDefaultGroup extends LabyGroup {
	
	public LabyDefaultGroup() {
		super(0, "user", "User", null, 'f', "USER", "NONE", null, null);
	}

	public LabyGroup init() {
		return super.init();
	}
	
	@Override
	public void renderBadge(double x, double y, double width, double height, boolean small) {
		ResourceLocation texture = small ? ModTextures.BADGE_GROUP_SMALL : ModTextures.BADGE_GROUP;
	
		GlStateManager.enableBlend();
	
		Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
		LabyMod.getInstance().getDrawUtils().drawTexture(x, y, 255.0D, 255.0D, 8.0D, 8.0D, 1.1F);
	}
}
